/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity implements
        TimerListFragment.OnFragmentInteractionListener,
        TimerEditFragment.OnFragmentInteractionListener,
        PrefsFragment.OnFragmentInteractionListener {

    public static int PREFS_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Fragment timerList = TimerListFragment.newInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, timerList);

        transaction.commit();

    }

    @Override
    public void onFragmentInteraction(String action) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(TimerListFragment.ACTION_NEW.equals(action)) {
            TimerEditFragment edit = new TimerEditFragment();
            transaction.replace(R.id.fragment_container, edit);
            transaction.addToBackStack(null);
            transaction.commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(R.string.add_timer);
        }
        else if(TimerListFragment.ACTION_EDIT.equals(action)) {
            TimerListFragment list = (TimerListFragment)manager.findFragmentById(
                    R.id.fragment_container);
            TimerEditFragment edit = new TimerEditFragment();
            Bundle args = new Bundle();
            args.putInt(TimerManager.EXTRA_ID, list.getCurrentTimer().getId());
            edit.setArguments(args);
            transaction.replace(R.id.fragment_container, edit);
            transaction.addToBackStack(null);
            transaction.commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(R.string.edit_timer);
        }
        else if(TimerEditFragment.ACTION_SAVE.equals(action) ||
                TimerEditFragment.ACTION_SAVE_RUN.equals(action)) {
            if(TimerEditFragment.ACTION_SAVE_RUN.equals(action)) {
                TimerEditFragment edit = (TimerEditFragment)manager.findFragmentById(
                        R.id.fragment_container);
                Timer timer = edit.getTimer();
                TimerManager.startTimer(this, timer);
            }

            onBackPressed();
        }
        else if(PrefsFragment.ACTION_DELETE_ALL.equals(action)) {
            TimerManager.deleteAll(this);
            onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dupa_count, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            showMainFragment();
        }
    }

    private void showMainFragment() {
        /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = new TimerListFragment();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();*/
        getSupportFragmentManager().popBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitle(R.string.app_name);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        Bundle bundle = data.getExtras();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (bundle != null) {
            Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            String ringtoneString = "";

            if (uri != null) {
                ringtoneString = uri.toString();
            }

            sharedPrefs.edit().putString(PrefsFragment.KEY_RINGTONE, ringtoneString).apply();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (id == R.id.action_settings) {
            Fragment fragment = new PrefsFragment();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null);

            transaction.commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(getString(R.string.action_settings));
            return true;
        }
        else if(id == R.id.action_reset_all) {
            TimerManager.resetAll(this);
            return true;
        }
        else if(id == android.R.id.home) {
            showMainFragment();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
