/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Field;


public class AlarmAlert extends Activity {
    private static String TAG = AlarmAlert.class.getName();
    private Timer mTimer = null;
    int mVolumeBehavior = 0;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Timer t = TimerManager.getTimer(context, intent.getIntExtra(TimerManager.EXTRA_ID, 0));
            if(t.getId() == mTimer.getId()) {
                dismiss(true);
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mTimer = TimerManager.getTimer(this,
                getIntent().getIntExtra(TimerManager.EXTRA_ID, 0));

        String vol = PreferenceManager.getDefaultSharedPreferences(this).getString(
                PrefsFragment.KEY_VOLUME_BEHAVIOR, PrefsFragment.DEFAULT_VOLUME_BEHAVIOR);
        mVolumeBehavior = Integer.parseInt(vol);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        int flags = getFlagsForVersion();
        getWindow().addFlags(flags);
        updateLayout();
        registerReceiver(mReceiver, new IntentFilter(TimerManager.ALARM_KILLED));
    }

    /**
     * Use reflection to add only the flags that are available in the
     * current version of Android, starting at Android 1.5+
     *
     * @return the flags available to the current platform
     */
    private int getFlagsForVersion() {
        final String possibleFlags[] = new String[] {
                "FLAG_SHOW_WHEN_LOCKED",
                "FLAG_DISMISS_KEYGUARD",
                "FLAG_TURN_SCREEN_ON"
        };

        int flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        for(String flag:possibleFlags) {
            try {
                Field field = WindowManager.LayoutParams.class.getField(flag);
                int value = field.getInt(null);
                flags |= value;
            }
            catch(NoSuchFieldException | IllegalAccessException e) { }
        }

        return flags;
    }

    public void setTitle() {
        TextView title = (TextView)findViewById(R.id.alertTitle);
        title.setText(mTimer.getDescriptionFormatted());
    }

    public View inflateView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.activity_alarm_alert, null);
    }

    public void updateLayout() {
        LayoutInflater inflater = LayoutInflater.from(this);
        setContentView(inflateView(inflater));
        Button button = (Button)findViewById(R.id.dismiss);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss(false);
            }
        });

        setTitle();
    }

    public void dismiss(boolean killed) {
        if(!killed) {
            NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancel(mTimer.getId());
            stopService(new Intent(this, AlarmService.class));
        }
        finish();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mTimer = TimerManager.getTimer(this,
                intent.getIntExtra(TimerManager.EXTRA_ID, 0));
        setTitle();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // Do this on key down to handle a few of the system keys.
        boolean up = event.getAction() == KeyEvent.ACTION_UP;
        switch (event.getKeyCode()) {
            // Volume keys and camera keys dismiss the alarm
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_CAMERA:
            case KeyEvent.KEYCODE_FOCUS:
                if (up) {
                    switch (mVolumeBehavior) {
                        case 1:
                            dismiss(false);
                            break;

                        default:
                            break;
                    }
                }
                return true;
            default:
                break;
        }
        return super.dispatchKeyEvent(event);
    }
}
