/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.synicworld.dupacount;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class TimerProvider extends ContentProvider {
    private SQLiteOpenHelper helper;
    private static final int TIMERS = 1;
    private static final int TIMER_ID = 2;
    private static final UriMatcher sUrlMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    private static final String TAG = "TimerProvider";
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);

    static {
        sUrlMatcher.addURI("com.synicworld.dupacount", "timer", TIMERS);
        sUrlMatcher.addURI("com.synicworld.dupacount", "timer/#", TIMER_ID);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "timers.db";
        private static final int DATABASE_VERSION = 2;

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE timers(\n" +
                    "        _id INTEGER PRIMARY KEY,\n" +
                    "        description VARCHAR(255) NOT NULL DEFAULT \"\", \n" +
                    "        seconds INTEGER UNSIGNED NOT NULL, \n" +
                    "        time_started INTEGER UNSIGNED NOT NULL DEFAULT 0, \n" +
                    "        running INTEGER UNSIGNED NOT NULL DEFAULT 0, \n" +
                    "        time_left INTEGER UNSIGNED NOT NULL DEFAULT 0,\n" +
                    "        ord INTEGER UNSIGNED NOT NULL DEFAULT 0)");

            String insertMe = "INSERT INTO timers (description, " +
                    "seconds) VALUES ";
            db.execSQL(insertMe + "(\"25 Minutes\", 1500)");
            db.execSQL(insertMe + "(\"30 Minutes\", 1800)");
            db.execSQL(insertMe + "(\"45 Minutes\", 2700)");
            db.execSQL(insertMe + "(\"1 Hour\", 3600)");
            db.execSQL(insertMe + "(\"10 Seconds\", 10)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if(oldVersion == 1 && newVersion == 2) {
                db.execSQL("ALTER TABLE timers ADD ord INTEGER UNSIGNED NULL DEFAULT 0");
                Cursor cur = db.rawQuery("SELECT _id FROM timers", null);
                int count = 0;
                if(cur.moveToFirst()) {
                    while(!cur.isAfterLast()) {
                        int id = cur.getInt(0);
                        db.execSQL("UPDATE timers SET ord=" + count + " WHERE _id=" + id);
                        count++;
                        cur.moveToNext();
                    }
                }
            }
        }
    }

    public TimerProvider() {
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = helper.getWritableDatabase();
        int count;

        @SuppressWarnings("unused")
        long rowId = 0;
        switch(sUrlMatcher.match(uri)) {
            case TIMERS:
                count = db.delete("timers", where, whereArgs);
                break;
            case TIMER_ID:
                String segment = uri.getPathSegments().get(1);
                if(TextUtils.isEmpty(where)) {
                    where = "_id=" + segment;
                }
                else {
                    where = "_id=" + segment + " AND (" + where + ")";
                }
                count = db.delete("timers", where, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Cannot delete from URL: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        int match = sUrlMatcher.match(uri);
        switch(match) {
            case TIMERS:
                return "vdn.android.cursor.dir/timers";
            case TIMER_ID:
                return "vdn.android.cursor.item/timers";
            default:
                throw new IllegalArgumentException("Unknown URL");
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if(sUrlMatcher.match(uri) != TIMERS) {
            throw new IllegalArgumentException("Cannot insert into url: " + uri);
        }

        ContentValues values;
        if(initialValues != null) {
            values = new ContentValues(initialValues);
        }
        else {
            values = new ContentValues();
        }

        if(!values.containsKey(Timer.Columns.DESCRIPTION))
            values.put(Timer.Columns.DESCRIPTION, "");

        if(!values.containsKey(Timer.Columns.SECONDS))
            values.put(Timer.Columns.SECONDS, 10);

        if(!values.containsKey(Timer.Columns.TIME_STARTED))
            values.put(Timer.Columns.TIME_STARTED, 0);

        if(!values.containsKey(Timer.Columns.RUNNING))
            values.put(Timer.Columns.RUNNING, 0);

        if(!values.containsKey(Timer.Columns.TIME_LEFT))
            values.put(Timer.Columns.TIME_LEFT, 0);

        if(!values.containsKey(Timer.Columns.ORDER))
            values.put(Timer.Columns.ORDER, 0);

        SQLiteDatabase db = helper.getWritableDatabase();
        long rowId = db.insert("timers", Timer.Columns.DESCRIPTION, values);
        if(rowId <= 0) {
            throw new SQLException("Failed to insert timer_list_row into " + uri);
        }

        Uri newUrl = ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, rowId);
        getContext().getContentResolver().notifyChange(uri, null);

        return newUrl;
    }

    @Override
    public boolean onCreate() {
        helper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        int match = sUrlMatcher.match(uri);
        switch(match) {
            case TIMERS:
                qb.setTables("timers");
                break;
            case TIMER_ID:
                qb.setTables("timers");
                qb.appendWhere("_id=");
                qb.appendWhere(uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }

        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor ret = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        if(ret == null) {
            if(DEBUG) Log.d(TAG, "Timer Query failed");
        }
        else {
            ret.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return ret;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int count;
        long rowId = 0;
        int match = sUrlMatcher.match(uri);
        SQLiteDatabase db = helper.getWritableDatabase();
        switch(match) {
            case TIMERS:
                count = db.update("timers", values, null, null);
                break;
            case TIMER_ID:
                String segment = uri.getPathSegments().get(1);
                rowId = Long.parseLong(segment);
                count = db.update("timers", values, "_id=" + rowId, null);
                break;
            default:
                throw new UnsupportedOperationException("Cannot update url: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

}