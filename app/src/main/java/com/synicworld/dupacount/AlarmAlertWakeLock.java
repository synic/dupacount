/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.content.Context;
import android.os.PowerManager;

public class AlarmAlertWakeLock {
    private static PowerManager.WakeLock sWakeLock = null;
    private static PowerManager sPowerManager = null;

    public static void acquireCpuWakeLock(Context context) {
        if(sWakeLock == null) {
            sPowerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            sWakeLock = sPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK|
                    PowerManager.ACQUIRE_CAUSES_WAKEUP|
                    PowerManager.ON_AFTER_RELEASE, "AlarmAlertWakeLock");
            sWakeLock.acquire();
        }
    }

    public static void releaseCpuLock() {
        if(sWakeLock != null) {
            sWakeLock.release();
            sWakeLock = null;
        }
    }
}
