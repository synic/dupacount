/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.RingtonePreference;

import com.synicworld.dupacount.utils.PreferenceFragment;


public class PrefsFragment extends PreferenceFragment {
    public static String KEY_LED = "use_led";
    public static boolean DEFAULT_LED = true;

    public static String KEY_RINGTONE = "sound";
    public static String KEY_VIBRATE = "vibrate";

    public static String DEFAULT_ALARM = "content://settings/system/activity_alarm_alert";
    public static boolean DEFAULT_VIBRATE = true;

    public static String KEY_VOLUME_BEHAVIOR = "volume_behavior";
    public static String DEFAULT_VOLUME_BEHAVIOR = "2";

    public static int RESULT_DELETE_ALL = Activity.RESULT_FIRST_USER + 3;

    private Preference mDeleteAll;
    private RingtonePreference mRingtone;
    public static final String ACTION_DELETE_ALL = "com.synicworld.dupacount.DELETE_ALL";
    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.layout.preferences);
        mDeleteAll = (Preference)findPreference("delete_all");
        mDeleteAll.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(getActivity()).setTitle(R.string.delete_all)
                        .setMessage(getString(R.string.delete_sure))
                        .setNegativeButton(getString(R.string.no), null)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mListener.onFragmentInteraction(ACTION_DELETE_ALL);
                                dialog.dismiss();
                            }
                        }).show();
                return false;
            }
        });

        mRingtone = (RingtonePreference)findPreference("sound");
        mRingtone.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                System.out.println(newValue);
                return false;
            }
        });
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
