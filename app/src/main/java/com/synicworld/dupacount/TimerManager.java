/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimerTask;
import java.util.TreeMap;

public class TimerManager {
    public static String EXTRA_ID = "dc:intent.extra.timer_id";
    public static String CLEAR_NOTIFICATION = "com.synicworld.dupacount.CLEAR_NOTIFICATION";
    public static String ALARM_KILLED = "com.synicworld.dupacount.ALARM_KILLED";
    public static String TIMER_ALERT_ACTION = "com.synicworld.dupacount.ALARM_ALERT";

    private static ArrayList<TimerObserver> sObservers = new ArrayList<TimerObserver>(1);
    private static TreeMap<Integer, Timer> sTimers = null;
    private static Handler sHandler = new Handler();
    private static java.util.Timer sJavaTimer = null;
    private static SaveTimer sSaveTimer;

    public static interface TimerObserver {
        void timerChanged(Timer timer);
        void reloadAll();
        void timerDeleted(Timer timer);
    }

    public static synchronized void save(Context context, Timer timer) {
        if(timer.getId() == -1) {
            Uri uri = addTimer(context);
            String segment = uri.getPathSegments().get(1);
            timer.setId(Integer.parseInt(segment));
            sTimers.put(timer.getId(), timer);
        }

        ContentResolver resolver = context.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(Timer.Columns.DESCRIPTION, timer.getDescription());
        values.put(Timer.Columns.SECONDS, timer.getSeconds());
        values.put(Timer.Columns.TIME_STARTED, timer.getTimeStarted());
        values.put(Timer.Columns.RUNNING, timer.isRunning() ? 1 : 0);
        values.put(Timer.Columns.TIME_LEFT, timer.getSecondsLeft());
        values.put(Timer.Columns.ORDER, timer.getOrder());

        resolver.update(ContentUris.withAppendedId(
                Timer.Columns.CONTENT_URI, timer.getId()), values, null, null);
        notifyChanged(timer);
    }

    public static synchronized void saveAll(Context context) {
        if(sJavaTimer != null) return;
        sJavaTimer = new java.util.Timer(false);
        sSaveTimer = new SaveTimer(context);
        sJavaTimer.schedule(sSaveTimer, 300);
    }

    public static void startTimer(Context context, Timer timer) {
        timer.start();
        save(context, timer);
        notifyChanged(timer);
        scheduleNextAlarm(context);
    }

    public static void stopTimer(Context context, Timer timer) {
        timer.stop();
        save(context, timer);
        notifyChanged(timer);
        scheduleNextAlarm(context);
    }

    public static void resetTimer(Context context, Timer timer) {
        timer.reset();
        save(context, timer);
        notifyChanged(timer);
        scheduleNextAlarm(context);
    }

    public static synchronized void addObserver(TimerObserver o) {
        sObservers.add(o);
    }
    public static synchronized void removeObserver(TimerObserver o) {
        sObservers.remove(o);
    }

    public static void cancelTimer(Context context) {
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(
                context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        mgr.cancel(pi);
    }

    public static void scheduleNextAlarm(Context context) {
        Timer timer = nearestTimerTimeout(context);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        if(null != timer) {
            i.putExtra(EXTRA_ID, timer.getId());
        }

        PendingIntent pi = PendingIntent.getBroadcast(
                context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);

        if(null != timer) {
            mgr.set(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + (timer.getSecondsLeft() * 1000), pi);
        }
        else {
            mgr.cancel(pi);
        }
    }

    protected static synchronized Timer nearestTimerTimeout(Context context) {
        Timer least = null;

        for(Timer timer : sTimers.values()) {
            if(timer.isRunning()) {
                if(null == least) least = timer;
                else if(timer.getSecondsLeft() < least.getSecondsLeft()) least = timer;
            }
        }

        return least;
    }

    public static synchronized void resetAll(Context context) {
        ContentResolver resolver = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(Timer.Columns.RUNNING, 0);
        values.put(Timer.Columns.TIME_STARTED, 0);
        resolver.update(Timer.Columns.CONTENT_URI, values, null, null);

        for(Timer timer : sTimers.values()) {
            timer.setRunning(false);
            timer.setStartTime(0);
        }

        notifyReloadAll();
        cancelTimer(context);
    }

    public static synchronized void pauseAll(Context context) {
        for(Timer timer : sTimers.values()) {
            if(timer.isRunning()) {
                timer.togglePause();
                save(context, timer);
            }
        }

        notifyReloadAll();
        cancelTimer(context);
    }

    protected static Uri addTimer(Context context) {
        ContentResolver resolver = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(Timer.Columns.DESCRIPTION, "New Timer");
        return resolver.insert(Timer.Columns.CONTENT_URI, values);
    }

    public static synchronized Timer getTimer(Context context, int id) {
        if(sTimers.containsKey(id)) {
            return sTimers.get(id);
        }
        else {
            return null;
        }
    }

    private static Timer readTimerFromDB(Context context, int id) {
        Timer timer = null;
        ContentResolver resolver = context.getContentResolver();

        Cursor cursor = resolver.query(ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, id),
                Timer.Columns.TIMER_QUERY_COLUMNS, null, null, null);
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                timer = new Timer(cursor);
            }
            cursor.close();
        }

        return timer;
    }

    public static synchronized void deleteAll(Context context) {
        ContentResolver resolver = context.getContentResolver();
        resolver.delete(Timer.Columns.CONTENT_URI, null, null);
        cancelTimer(context);
        sTimers.clear();
        notifyReloadAll();
    }

    public static void deleteTimer(Context context, Timer timer) {
        ContentResolver resolver = context.getContentResolver();
        Uri uri = ContentUris.withAppendedId(Timer.Columns.CONTENT_URI, timer.getId());
        resolver.delete(uri, "", null);
        if(sTimers.containsKey(timer.getId())) {
            sTimers.remove(timer.getId());
        }
        notifyDeleted(timer);
    }



    public static synchronized ArrayList<Timer> timers(Context context) {
        if(sTimers != null) {
            ArrayList<Timer> items = new ArrayList<Timer>(sTimers.values());
            Collections.sort(items, sTimerComparator);
            return items;
        }

        return readTimersFromDB(context);
    }

    public static void togglePause(Context context, Timer timer) {
        timer.togglePause();
        save(context, timer);
        notifyChanged(timer);
        scheduleNextAlarm(context);
    }

    public static synchronized ArrayList<Timer> readTimersFromDB(Context context) {
        ContentResolver resolver = context.getContentResolver();
        sTimers = new TreeMap<>();
        Cursor cur = timerCursor(resolver);
        if(cur.moveToFirst()) {
            while(!cur.isAfterLast()) {
                int id = cur.getInt(Timer.Columns.TIMER_ID_INDEX);
                Timer timer = readTimerFromDB(context, id);
                sTimers.put(timer.getId(), timer);
                cur.moveToNext();
            }
        }

        cur.close();

        ArrayList<Timer> items = new ArrayList<>(sTimers.values());
        Collections.sort(items, sTimerComparator);
        return items;
    }

    private static class SaveTimer extends TimerTask {
        private Context context;
        public SaveTimer(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            for(final Timer timer : sTimers.values()) {
                sHandler.post(new Runnable() {
                    public void run() {
                        save(context, timer);
                    }
                });
            }
            sJavaTimer = null;
        }
    }

    private static Cursor timerCursor(ContentResolver resolver) {
        return resolver.query(Timer.Columns.CONTENT_URI,
                Timer.Columns.TIMER_QUERY_COLUMNS,
                null,
                null,
                Timer.Columns.ORDER + " ASC");
    }

    private static synchronized void notifyReloadAll() {
        sHandler.post(new Runnable() {
            public void run() {
                for (TimerObserver ob : sObservers) {
                    ob.reloadAll();
                }
            }
        });
    }

    private static synchronized void notifyDeleted(final Timer timer) {
        sHandler.post(new Runnable() {
            public void run() {
                for (TimerObserver ob : sObservers) {
                    ob.timerDeleted(timer);
                }
            }
        });
    }

    private static synchronized void notifyChanged(final Timer timer) {
        sHandler.post(new Runnable() {
            public void run() {
                for (TimerObserver ob : sObservers) {
                    ob.timerChanged(timer);
                }
            }
        });
    }

    private static Comparator<Timer> sTimerComparator = new Comparator<Timer>() {
        @Override
        public int compare(Timer t1, Timer t2) {
            long o1 = t1.getOrder();
            long o2 = t2.getOrder();

            if (o1 > o2) return 1;
            else if (o1 == o2) return 0;
            else if (01 < o2) return -1;

            return 0;
        }
    };
}
