/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// based heavily on Android's official AlarmClock application
package com.synicworld.dupacount;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class AlarmService extends Service {
    private static final int ALARM_TIMEOUT_SECONDS = 10 * 60;
    private static final long [] sVibratePattern = new long[] {500, 500};
    private static final String TAG = "AlarmService";
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
    private boolean mPlaying = false;
    private Vibrator mVibrator;
    private MediaPlayer mPlayer;
    private Timer mTimer;
    private AudioManager mAudioManager;
    private SharedPreferences mPrefs;

    private static final int KILLER = 1000;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case KILLER:
                    sendKillBroadcast((Timer)msg.obj);
                    stopSelf();
                    break;
            }
        }
    };

    @Override
    public void onCreate() {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mVibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        AlarmAlertWakeLock.acquireCpuWakeLock(this);
    }

    @Override
    public void onDestroy() {
        stop();
        AlarmAlertWakeLock.releaseCpuLock();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent == null) {
            stopSelf();
            return START_STICKY;
        }
        
        mTimer = TimerManager.getTimer(this,
                intent.getIntExtra(TimerManager.EXTRA_ID, 0));
        
        if(mTimer == null) {
            stopSelf();
            return START_STICKY;
        }

        play(mTimer);

        return START_STICKY;
    }

    private void sendKillBroadcast(Timer c) {
        Intent alarmKilled = new Intent(TimerManager.ALARM_KILLED);
        alarmKilled.putExtra(TimerManager.EXTRA_ID, c.getId());
        sendBroadcast(alarmKilled);
    }

    private static final float IN_CALL_VOLUME = 0.125f;

    private void play(Timer c) {
        stop();

        Uri alert = null;
        alert = Uri.parse(mPrefs.getString(PrefsFragment.KEY_RINGTONE,
                PrefsFragment.DEFAULT_ALARM));
        if(DEBUG) Log.d(TAG, "alert: " + alert);

        mPlayer = new MediaPlayer();
        mPlayer.setOnErrorListener(new OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (DEBUG) Log.e(TAG, "Error occurred while playing audio.");
                mp.stop();
                mp.release();
                mPlayer = null;
                return true;
            }
        });

        try {
            if(mAudioManager.getMode() == AudioManager.MODE_IN_CALL)
            {
                if(DEBUG) Log.v(TAG, "Using in-call alarm");
                mPlayer.setVolume(IN_CALL_VOLUME, IN_CALL_VOLUME);
                setDataSourceFromResource(getResources(), mPlayer,
                        R.raw.in_call_alarm);
            }
            else {
                mPlayer.setDataSource(this, alert);
            }
            startAlarm(mPlayer);
        }
        catch(Exception ex) {
            if(DEBUG) Log.d(TAG, "Failed to play ringtone", ex);
        }

        if(mPrefs.getBoolean(PrefsFragment.KEY_VIBRATE,
                PrefsFragment.DEFAULT_VIBRATE)) {
            mVibrator.vibrate(sVibratePattern, 0);
        }

        enableKiller(c);
        mPlaying = true;
    }

    private void startAlarm(MediaPlayer player) throws java.io.IOException,
            IllegalArgumentException, IllegalStateException {
        player.setAudioStreamType(AudioManager.STREAM_ALARM);
        player.setLooping(true);
        player.prepare();
        player.start();
    }

    private void setDataSourceFromResource(Resources resources,
                                           MediaPlayer player, int res)
            throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if(afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    /**
     * Stops alarm audio
     */
    public void stop() {
        if(mPlaying) {
            mPlaying = false;

            if(mPlayer != null) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
            }

            mVibrator.cancel();
        }

        disableKiller();
    }

    private void enableKiller(Timer c) {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(KILLER, c),
                1000 * ALARM_TIMEOUT_SECONDS);
    }

    private void disableKiller() {
        mHandler.removeMessages(KILLER);
    }
}