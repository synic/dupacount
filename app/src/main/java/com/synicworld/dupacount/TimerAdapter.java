/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.synicworld.dupacount;

import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * The model for the SupaCountActivity ListView
 */
class TimerAdapter extends ArrayAdapter<Timer> implements
        Iterable<Timer>, TimerManager.TimerObserver
{
    private Activity mContext;
    private static final String TAG = "TimerAdapter";
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG) || true;

    @Override
    public void timerChanged(Timer timer) {

        if(DEBUG) {
            Log.d(TAG, "timerChanged() called for: " + timer.getDescription());
        }

        boolean found = false;
        int size = getCount();
        for(int i = 0; i < size; i++) {
            Timer check = getItem(i);
            if(check.getId() == timer.getId()) {
                found = true;
                break;
            }
        }

        if(!found) {
            add(timer);
            saveOrder();
        }

        notifyDataSetChanged();
    }

    public void reloadAll() {
        loadItems();
    }

    public void timerDeleted(Timer timer) {
        int size = getCount();
        for(int i = 0; i < size; i++) {
            Timer check = getItem(i);
            if(check.getId() == timer.getId()) {
                remove(check);
                break;
            }
        }
    }

    protected void saveOrder() {
        int count = 0;
        int size = getCount();
        for(int i = 0; i < size; i++) {
            Timer timer = getItem(i);
            timer.setOrder(count);
            count++;
        }

        TimerManager.saveAll(mContext);
    }

    /**
     * Sets up the model
     */
    public TimerAdapter(Activity context) {
        super(context, R.layout.timer_list_row_stopped);
        this.mContext = context;
        TimerManager.addObserver(this);
        loadItems();
    }

    /**
     * Clears the model, and sets it to the new array that was passed in
     *
     */
    public void loadItems() {
        clear();
        ArrayList<Timer> timers = TimerManager.timers(mContext);
        for(Timer timer:timers) {
            add(timer);
        }
    }

    /**
     * Returns a new iterator to implement iterable
     */
    public Iterator<Timer> iterator() {
        return new TimerAdapterIterator();
    }

    /**
     * Returns the View for a specific timer_list_row.
     *
     * In this case, it builds the view with the CountDown information and
     * layout/timer_list_row.xmlist_row.xml
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Timer timer = getItem(position);

        int inflateLayout = R.layout.timer_list_row_stopped;
        int inflateId = R.id.timer_list_row_stopped;

        if(timer.isRunning()) {
            inflateLayout = R.layout.timer_list_row_running;
            inflateId = R.id.timer_list_row_running;
        }
        else if(timer.getSecondsLeft() != timer.getSeconds()) {
            inflateLayout = R.layout.timer_list_row_paused;
            inflateId = R.id.timer_list_row_paused;
        }

        if(row == null || row.getId() != inflateId) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            row = inflator.inflate(inflateLayout, parent, false);
        }

        TextView description = (TextView)row.findViewById(R.id.timer_desc);
        TextView secondsLeft = (TextView)row.findViewById(R.id.timer_left);
        TextView seconds = (TextView)row.findViewById(R.id.timer_time);

        description.setText(timer.getDescriptionFormatted());
        secondsLeft.setText(timer.getSecondsLeftFormatted());
        seconds.setText(timer.getSecondsFormatted());

        return(row);
    }

    /**
     * Iterator class, allows you to use a foreach loop on the CountDownAdapter
     * @author synic
     *
     */
    public class TimerAdapterIterator implements Iterator<Timer> {
        int count;

        /**
         * Initializes the iterator
         */
        public TimerAdapterIterator() {
            this.count = 0;
        }

        /**
         * Returns the next item in the iterator
         */
        public Timer next() {
            Timer item = getItem(count);
            count ++;
            return item;
        }

        /**
         * Returns true if there is another item in the iterator
         */
        public boolean hasNext() {
            return count < getCount();
        }

        /**
         * Required for the Iterator interface, but not used.
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}