/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.synicworld.dupacount;

import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * The Timer object.  Represents every timer that the user
 * has created
 * @author Adam Olsen
 *
 */
public class Timer implements Parcelable {

    public static class Columns implements BaseColumns {
        public static final Uri CONTENT_URI =
                Uri.parse("content://com.synicworld.dupacount/timer");
        public static final String DESCRIPTION = "description";
        public static final String SECONDS = "seconds";
        public static final String TIME_STARTED = "time_started";
        public static final String RUNNING = "running";
        public static final String TIME_LEFT = "time_left";
        public static final String ORDER = "ord";

        public static final String DEFAULT_SORT_ORDER = _ID + "ASC";
        static final String[] TIMER_QUERY_COLUMNS = {
                _ID, DESCRIPTION, SECONDS, TIME_STARTED, RUNNING, TIME_LEFT, ORDER};

        public static final int TIMER_ID_INDEX = 0;
        public static final int TIMER_DESCRIPTION_INDEX = 1;
        public static final int TIMER_SECONDS_INDEX = 2;
        public static final int TIMER_TIME_STARTED_INDEX = 3;
        public static final int TIMER_RUNNING_INDEX = 4;
        public static final int TIMER_TIME_LEFT_INDEX = 5;
        public static final int TIMER_ORDER_INDEX = 6;
    }

    private static final String TAG = "Timer";
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
    private int mId;
    private long mSeconds;
    private String mDescription;
    private long mTimeStarted;
    private long mTimeLeft;
    private boolean mRunning;
    private long mOrder;

    /**
     * Creates a Timer object from a Cursor
     */
    public Timer(Cursor c) {
        mId = c.getInt(Columns.TIMER_ID_INDEX);
        mDescription = c.getString(Columns.TIMER_DESCRIPTION_INDEX);
        mSeconds = c.getLong(Columns.TIMER_SECONDS_INDEX);
        mTimeStarted = c.getLong(Columns.TIMER_TIME_STARTED_INDEX);
        mRunning = c.getInt(Columns.TIMER_RUNNING_INDEX) > 0;
        mTimeLeft = c.getLong(Columns.TIMER_TIME_LEFT_INDEX);
        setOrder(c.getLong(Columns.TIMER_ORDER_INDEX));
    }

    /**
     * Initializes the timer with an mId
     *
     * @param id 			The mId of the timer
     * @param description 	The mDescription
     * @param seconds 		Number of mSeconds the timer will last.
     */
    public Timer(int id, String description, long seconds)
    {
        init(id, description, seconds);
    }

    /**
     * Initializes the timer without an mId
     *
     * @param description	The mDescription
     * @param seconds		Number of mSeconds the timer will last
     */
    public Timer(String description, long seconds) {
        init(-1, description, seconds);
    }

    /**
     * Initializes the timer
     *
     * @param id			The mId
     * @param description	The mDescription
     * @param seconds		Number of mSeconds
     */
    protected void init(int id, String description, long seconds)
    {
        this.mId = id;
        this.mDescription = description;
        this.mSeconds = seconds;
        mTimeStarted = 0;
        mRunning = false;
        mTimeLeft = seconds;

        if(DEBUG) {
            Log.d(TAG, "Initializing timer: " + description);
        }
    }

    /**
     * Determines if one Timer is equal to another Timer by value
     */
    public boolean equals(Timer timer) {
        if(timer.mId != mId) return false;
        else if(!timer.mDescription.equals(mDescription)) return false;
        else if(timer.mSeconds != mSeconds) return false;
        else if(timer.mTimeLeft != mTimeLeft) return false;
        else if(timer.mTimeStarted != mTimeStarted) return false;
        else if(timer.mRunning != mRunning) return false;
        else if(timer.getOrder() != getOrder()) return false;

        return false;
    }

    /**
     * Starts this Timer
     *
     * Marks it as started, sets the time it started
     */
    public void start() {
        mRunning = true;
        mTimeStarted = System.currentTimeMillis();
    }

    /**
     * Stops the timer, sets mRunning as false and the mTimeStarted to 0
     */
    public void stop() {
        mRunning = false;
        mTimeStarted = 0;
    }

    /**
     * Parcelable interface
     */
    public static final Parcelable.Creator<Timer> CREATOR = new Parcelable.Creator<Timer>() {
        /**
         * Creates the timer from a Parcel
         */
        public Timer createFromParcel(Parcel in) {
            return new Timer(in);
        }

        /**
         * Creates a new array of timers
         */
        public Timer[] newArray(int size) {
            return new Timer[size];
        }
    };

    /**
     * Parcel constructor
     * @param in  The parcel to initialize the timer
     */
    private Timer(Parcel in) {
        readFromParcel(in);
    }

    /**
     * Writes the timer to a parcel
     *
     * @param out		The parcel to write the object to
     * @param flags		Not used at this time
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mId);
        out.writeString(mDescription);
        out.writeLong(mSeconds);
        out.writeLong(mTimeStarted);
        out.writeLong(mTimeLeft);
        out.writeInt(mRunning ? 1 : 0);
        out.writeLong(getOrder());
    }

    /**
     * Used by android, returns 0
     */
    public int describeContents() { return 0; }

    /**
     * Reads the timer from a parcel
     * @param in	The parcel
     */
    public void readFromParcel(Parcel in) {
        mId = in.readInt();
        mDescription = in.readString();
        mSeconds = in.readLong();
        mTimeStarted = in.readLong();
        mTimeLeft = in.readLong();
        mRunning = in.readInt() > 0;
        setOrder(in.readLong());
    }

    /**
     * Gets the timer mId
     *
     * @return the mId
     */
    public int getId()
    {
        // yes, I'm casting a long to an int here.  I'm not sure how else
        // to get around it, but Google does it in their AlarmClock application
        // as well.  Perhaps db.insert only returns the type long, but it never
        // gets to a value that's larger than an int.  I don't know.
        return (int) mId;
    }

    /**
     * Sets the mId from an int
     */
    protected void setId(int id) {
        this.mId = id;
    }

    /**
     * Returns the mSeconds this timer will last
     *
     * @return the mSeconds
     */
    public long getSeconds()
    {
        return mSeconds;
    }

    /**
     * Gets the mDescription of this timer
     *
     * @return the mDescription
     */
    public String getDescription()
    {
        return mDescription;
    }

    /**
     * Sets the number of mSeconds this timer will last
     *
     * @param seconds the mSeconds
     */
    public void setSeconds(long seconds)
    {
        this.mSeconds = seconds;
        this.mRunning = false;
        this.mTimeStarted = 0;
        this.mTimeLeft = seconds;
    }

    /**
     * Get the mRunning status of this timer
     * @return true if the timer is mRunning, false if not
     */
    public boolean isRunning() { return mRunning; }

    /**
     * Sets the timer as mRunning
     */
    public void setRunning(boolean running) {
        this.mRunning = running;
    }

    public long getOrder() {
        return mOrder;
    }

    public void setOrder(long order) {
        this.mOrder = order;
    }

    /**
     * Toggles pause
     */
    public void togglePause() {
        if(mTimeStarted <= 0) {
            start();
            return;
        }

        if(mRunning) {
            mTimeLeft = getSecondsLeft();
            mRunning = false;
        }
        else {
            mRunning = true;
            long diff = (mSeconds - mTimeLeft) * 1000;
            mTimeStarted = System.currentTimeMillis() - diff;
        }
    }

    /**
     * Sets the timer mDescription
     *
     * @param description the mDescription
     */
    public void setDescription(String description)
    {
        this.mDescription = description;
    }

    /**
     * Returns the formatted number of mSeconds
     *
     * @return The mSeconds in HH:MM:SS format
     */
    public String getSecondsFormatted()
    {
        return this.getFormatted(this.mSeconds);
    }


    /**
     * Gets the number of mSeconds left formatted
     *
     * @return  The number of mSeconds left in HH:MM:SS format
     */
    public String getSecondsLeftFormatted()
    {
        return getFormatted(getSecondsLeft());
    }

    /**
     * Gets the number of mSeconds left on this timer
     */
    public long getSecondsLeft() {
        if(mTimeStarted <= 0) { return mSeconds; }
        if(!mRunning) { return mTimeLeft; }
        return mSeconds - ((System.currentTimeMillis() - mTimeStarted) / 1000);
    }

    /**
     * Gets the number of mSeconds in HH:MM:SS format
     *
     * @param seconds  The mSeconds to format
     * @return	The mSeconds in HH:MM:SS format
     */
    public static String getFormatted(long seconds) {
        long info[] = getHoursMinutesSeconds(seconds);
        long hours = info[0];
        long minutes = info[1];
        long secs = info[2];

        return (((hours < 10) ? "0" + hours : hours)
                + ":" + (minutes < 10 ? "0":"") + minutes
                + ":" + (secs < 10 ? "0":"") + secs);
    }

    public String getDescriptionFormatted() {
        if(mDescription == null || "".equals(mDescription)) {
            return getFormatted(getSeconds());
        }
        else {
            return mDescription;
        }
    }

    /**
     * Gets the hours, minutes, and mSeconds left
     *
     * @return the hours, minutes, and mSeconds left in an array
     */
    public long[] getHMSLeft() {
        return getHoursMinutesSeconds(getSecondsLeft());
    }

    /**
     * Gets the hours, minutes, and mSeconds
     *
     * @return  The hours, minutes, and mSeconds in an array
     */
    public long[] getHMS() {
        return getHoursMinutesSeconds(mSeconds);
    }

    /**
     * Generates the hours, minutes, and mSeconds from the specified mSeconds
     *
     * @param seconds the number of mSeconds as an long
     * @return a three element array containing the hours, minutes, and mSeconds
     */
    private static long[] getHoursMinutesSeconds(long seconds)
    {
        long hours = seconds / 3600;
        long minutes = (seconds / 60) - (hours * 60);
        long secs = seconds % 60;


        if(minutes == 60)  {
            if(hours > 1) hours += 1;
            minutes = 0;
        }

        if(seconds == 60) {
            if(minutes > 1) minutes += 1;
            seconds = 0;
        }

        return new long[] {hours, minutes, secs};
    }

    /**
     * Gets the mTimeStarted of this timer
     */
    public long getTimeStarted() {
        return mTimeStarted;
    }

    /**
     * Sets the start time
     */
    public void setStartTime(long time) {
        mTimeStarted = time;
    }

    /**
     * Resets the timer.
     *
     * Sets secondsLeft to the value in mSeconds
     */
    public void reset()
    {
        mRunning = false;
        mTimeStarted = 0;
    }
}