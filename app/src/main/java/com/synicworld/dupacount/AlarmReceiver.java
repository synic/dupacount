/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

/**
 * Receives the Alarm broadcast from the AlarmManager when a CountDown 
 * completes.
 *
 * This code is based on, if not entirely copied from, Android's 
 * stock AlarmClock application.  Code for their AlarmClock application can
 * be located at this address: http://android.git.kernel.org/  
 *
 * @author Adam Olsen
 *
 */
public class AlarmReceiver extends BroadcastReceiver {
    private SharedPreferences mPrefs;
    private NotificationCompat.Builder mBuilder;

    /**
     * Called by the AlarmManager when a timeout occurs
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (mPrefs == null) {
            mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        }
        NotificationManager nm = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);

        Timer timer = TimerManager.getTimer(context, intent.getIntExtra(TimerManager.EXTRA_ID, 0));
        if (timer == null) return;

        initBuilder(context, timer);

        if (TimerManager.CLEAR_NOTIFICATION.equals(intent.getAction())) {
            context.stopService(new Intent(context, AlarmService.class));
            nm.cancel(timer.getId());
            return;
        }
        else if (TimerManager.ALARM_KILLED.equals(intent.getAction())) {
            updateNotification(context, timer);
            return;
        }

        AlarmAlertWakeLock.acquireCpuWakeLock(context);
        dispatchIntent(context, intent, timer);
    }

    private void initBuilder(Context context, Timer timer) {
        // create pending intent
        Intent clearAll = new Intent(context, AlarmReceiver.class);
        clearAll.setAction(TimerManager.CLEAR_NOTIFICATION);
        clearAll.putExtra(TimerManager.EXTRA_ID, timer.getId());


        PendingIntent pi = PendingIntent.getBroadcast(context, timer.getId(), clearAll, 0);

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.icon);

        // create the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.status_icon)
                .setContentTitle(timer.getDescriptionFormatted())
                .setContentText(context.getString(R.string.timer_complete))
                .setContentIntent(pi)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setVibrate(new long[0])
                .addAction(android.R.drawable.ic_delete, "Dismiss", pi)
                .setLargeIcon(icon)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        if(mPrefs.getBoolean(PrefsFragment.KEY_LED, PrefsFragment.DEFAULT_LED)) {
            builder.setLights(0xFF00FF00, 500, 500);
        }

        mBuilder = builder;
    }

    private void dispatchIntent(Context context, Intent intent, Timer timer) {
        context.sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));

        timer.reset();
        TimerManager.save(context, timer);
        TimerManager.scheduleNextAlarm(context);

        Intent i = new Intent(context, AlarmService.class);
        i.putExtra(TimerManager.EXTRA_ID, timer.getId());

        NotificationManager nm = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);

        nm.notify(timer.getId(), mBuilder.build());
        context.startService(i);

        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if(km.inKeyguardRestrictedInputMode()) {
            Intent ai = new Intent(context, AlarmAlertFullScreen.class);
            ai.putExtra(TimerManager.EXTRA_ID, timer.getId());
            ai.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            context.startActivity(ai);
        }
    }

    private void updateNotification(Context context, Timer timer) {
        NotificationManager nm = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        if(timer == null) return;

        Intent i = new Intent(context, MainActivity.class);
        i.putExtra(TimerManager.EXTRA_ID, timer.getId());

        PendingIntent pi = PendingIntent.getActivity(context, timer.getId(), i, 0);
        mBuilder.setContentIntent(pi);
        mBuilder.setContentTitle(timer.getDescriptionFormatted());

        nm.notify(timer.getId(), mBuilder.build());
    }
}