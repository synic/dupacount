/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;
import com.synicworld.dupacount.utils.TouchListView;

import java.util.TimerTask;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class TimerListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;
    private TimerAdapter mModel = null;
    private Handler mHandler = new Handler();
    private java.util.Timer mThread = new java.util.Timer(false);
    private TouchListView mList;
    private static final String TAG = TimerListFragment.class.getName();
    public static final String ACTION_NEW = "com.synicworld.dupacount.ACTION_NEW";
    public static final String ACTION_EDIT = "com.synicworld.dupacount.ACTION_EDIT";
    private Timer mCurrentTimer;

    public static TimerListFragment newInstance() {
        TimerListFragment fragment = new TimerListFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TimerListFragment() {
    }

    public TimerAdapter getModel() { return mModel; }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle icicle) {
        super.onActivityCreated(icicle);
        if(mModel == null) {
            mModel = new TimerAdapter(getActivity());
            mThread.schedule(new TimerThread(), 0, 1000L);
        }

        mList.setAdapter(mModel);
        registerForContextMenu(mList);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo info) {
        new MenuInflater(getActivity().getApplication()).inflate(R.menu.main_context, menu);
        menu.setHeaderTitle(getString(R.string.timer));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        Timer timer = mModel.getItem(info.position);

        switch(item.getItemId()) {
            case R.id.reset_timer:
                TimerManager.resetTimer(getActivity(), timer);
                return true;
            case R.id.edit_timer:
                mCurrentTimer = timer;
                mListener.onFragmentInteraction(ACTION_EDIT);
                return true;
            case R.id.delete_timer:
                timer.stop();
                mModel.remove(timer);
                TimerManager.deleteTimer(getActivity(), timer);
                return true;
        }

        return super.onContextItemSelected(item);
    }

    public Timer getCurrentTimer() { return mCurrentTimer; }

    @Override
    public void onDestroy() {
        mThread.cancel();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle icicle) {
        View view = inflater.inflate(R.layout.fragment_timer_list, group, false);
        mList = (TouchListView)view.findViewById(R.id.list);
        mList.setDropListener(mOnDrop);
        mList.setOnItemClickListener(this);

        FloatingActionButton fab = (FloatingActionButton)view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mListener.onFragmentInteraction(ACTION_NEW);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private TouchListView.DropListener mOnDrop = new TouchListView.DropListener() {
        @Override
        public void drop(int from, int to) {
            Timer timer = mModel.getItem(from);
            mModel.remove(timer);
            mModel.insert(timer, to);
            mModel.saveOrder();
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView l, View v, int position, long id) {
        TimerManager.togglePause(getActivity(), mModel.getItem(position));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    class TimerThread extends TimerTask {
        @Override
        public synchronized void run() {
            int size = mModel.getCount();
            boolean refresh = false;
            for(int i = 0; i < size; i++) {
                Timer timer = mModel.getItem(i);
                if(timer.isRunning()) {
                    refresh = true;
                    if(timer.getSecondsLeft() <= 0) {
                        timer.stop();
                        TimerManager.save(getActivity(), timer);
                    }
                }
            }

            if(refresh) {
                mHandler.post(
                        new Runnable() {
                            public void run() {
                                mModel.notifyDataSetChanged();
                            }
                        }
                );
            }
        }
    }
}
