/*
 * DupaCount is a multi-countdown timer.
 * Copyright (C) 2014 Adam Olsen <arolsen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.synicworld.dupacount;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Formatter;

public class TimerEditFragment extends Fragment {
    public static String ACTION_SAVE = "com.synicworld.dupacount.ACTION_SAVE";
    public static String ACTION_SAVE_RUN = "com.synicworld.dupacount.ACTION_SAVE_RUN";
    private Timer mTimer = null;
    private boolean mEditing = false;
    private int mTime;
    private TextView mHour;
    private TextView mMinute;
    private TextView mSecond;
    private EditText mName;
    private OnFragmentInteractionListener mListener;
    private View mSave;
    private View mSaveRun;
    private boolean mStartedEditing = false;

    public TimerEditFragment() {

    }

    @Override
    public void onActivityCreated(Bundle icicle) {
        super.onActivityCreated(icicle);
        Bundle arguments = getArguments();
        if(arguments != null) {
            int id = getArguments().getInt(TimerManager.EXTRA_ID, 0);
            if (id > 0) {
                mEditing = true;
                mTimer = TimerManager.getTimer(getActivity(), id);
                if(mTimer.getDescription() != null && !"".equals(mTimer.getDescription())) {
                    mName.setText(mTimer.getDescription());
                }

                setTimeFromTimer(mTimer);

            } else {
                mEditing = false;
                mTimer = null;
                mTime = 0;
            }
        }
    }

    private void setTimeFromTimer(Timer timer) {
        long[] hms = timer.getHMS();
        Formatter formatter = new Formatter();
        String time = formatter.format("%02d%02d%02d", hms[0], hms[1], hms[2]).toString();
        mTime = Integer.valueOf(time);
        setTimeText();
    }

    private void setTimeText() {
        Formatter formatter = new Formatter();
        String time = formatter.format("%06d", mTime).toString();
        String hours = time.substring(0, 2);
        String minutes = time.substring(2, 4);
        String seconds = time.substring(4, 6);

        mHour.setText(hours);
        mMinute.setText(minutes);
        mSecond.setText(seconds);

        if(getSeconds() == 0) {
            mSave.setVisibility(View.GONE);
            mSaveRun.setVisibility(View.GONE);
        }
        else {
            mSave.setVisibility(View.VISIBLE);
            mSaveRun.setVisibility(View.VISIBLE);
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    private long getSeconds() {
        Formatter formatter = new Formatter();
        String time = formatter.format("%06d", mTime).toString();
        int hours = Integer.valueOf(time.substring(0, 2));
        int minutes = Integer.valueOf(time.substring(2, 4));
        int secs = Integer.valueOf(time.substring(4, 6));

        long seconds = hours * 3600;
        seconds += minutes * 60;
        seconds += secs;
        return seconds;
    }

    private void appendDigit(int digit) {
        if(!mStartedEditing) mTime = 0;
        mStartedEditing = true;

        String time = Integer.toString(mTime);
        if(time.length() > 5) return;

        time = time + digit;
        mTime = Integer.valueOf(time);
        setTimeText();
    }

    private void deleteDigit() {
        if(!mStartedEditing) {
            mTime = 0;
            setTimeText();
            mStartedEditing = true;
            return;
        }

        String time = Integer.toString(mTime);
        if(time.length() == 1 && "0".equals(time)) return;

        if(time.length() - 1 == 0) {
            mTime = 0;
        }
        else {
            time = time.substring(0, time.length() - 1);
            mTime = Integer.valueOf(time);
        }
        setTimeText();
    }

    public Timer getTimer() { return mTimer; }

    private void save(boolean run) {
        if(mTimer != null) {
            TimerManager.stopTimer(getActivity(), mTimer);
        }

        if(!mEditing) {
            mTimer = new Timer(mName.getText().toString(), getSeconds());
        }

        mTimer.setDescription(mName.getText().toString());
        mTimer.setSeconds(getSeconds());
        TimerManager.save(getActivity(), mTimer);

        if(run) {
            mListener.onFragmentInteraction(ACTION_SAVE_RUN);
        }
        else {
            mListener.onFragmentInteraction(ACTION_SAVE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle icicle) {
        View view = inflater.inflate(R.layout.fragment_timer_edit, group, false);

        mHour = (TextView)view.findViewById(R.id.hours);
        mMinute = (TextView)view.findViewById(R.id.minutes);
        mSecond = (TextView)view.findViewById(R.id.seconds);
        mName = (EditText)view.findViewById(R.id.name);
        View delete = view.findViewById(R.id.delete);

        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteDigit();
            }
        });

        mSave = view.findViewById(R.id.save);
        mSaveRun = view.findViewById(R.id.save_run);

        mSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                save(false);
            } });

        mSaveRun.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                save(true);
            } });

        for(int i = 0; i <= 9; i++) {
            try {
                final int digit = i;
                Field field = R.id.class.getField("button" + i);
                int id = field.getInt(R.id.class);
                View button = view.findViewById(id);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        appendDigit(digit);
                    }
                });
            }
            catch(NoSuchFieldException nsfe) { }
            catch(IllegalAccessException iae) { }
        }

        setTimeText();

        return view;
    }
}